package com.dev.mgaudiofile.joblist;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class Company {      //model layer

    private UUID mId;
    private String mCompanyName;
    private String mJobTitle;
    private String mDateApplied;
    private String mStatus;
    private int mStarFlag;

    private String mSkillsDesired;
    private String mCompanyNotes;

    //--- This helper idea is coupled to setStatus and the strings resource file!
    private int mStatusHelper;
    //this list must match spinner resource strings file
    private static String[] mStatusArr = new String[] {"Select", "Job Offered",
            "Pending", "1st Interview", "2nd Interview", "3rd Interview",
            "4th Interview", "Rejected"
    };
    private static List<String> mStatusList = Arrays.asList(mStatusArr);

    Company()
    {
        this(UUID.randomUUID());
    }

    public Company(UUID id) {
        mId = id;
    }

    public UUID getId() {
        return mId;
    }

    public String getCompanyName() {
        return mCompanyName;
    }

    public void setCompanyName(String mCompanyName) {
        this.mCompanyName = mCompanyName;
    }

    public String getJobTitle() {
        return mJobTitle;
    }

    public void setJobTitle(String mJobTitle) {
        this.mJobTitle = mJobTitle;
    }

    public String getDateApplied() {
        return mDateApplied;
    }

    public void setDateApplied(String mDateApplied) {
        this.mDateApplied = mDateApplied;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
        setStatusHelper(mStatusList.indexOf(status));
    }

    public int getStatusHelper() {
        switch (mStatusHelper) {    //switching precedence of interview numbers e.g. 4th interview takes higher precedence
            case 3:
                return 6;
            case 4:
                return 5;
            case 5:
                return 4;
            case 6:
                return 3;
        }
        return mStatusHelper;
    }

    private void setStatusHelper(int idx) { mStatusHelper = idx; }

    public void setStarFlag(int starValue) { mStarFlag = starValue; }

    public int getStarFlag() { return mStarFlag; }

    public String getSkillsDesired() { return mSkillsDesired; }

    public void setSkillsDesired(String mSkillsDesired) { this.mSkillsDesired = mSkillsDesired; }

    public String getCompanyNotes() { return mCompanyNotes; }

    public void setCompanyNotes(String mCompanyNotes) { this.mCompanyNotes = mCompanyNotes; }
}
