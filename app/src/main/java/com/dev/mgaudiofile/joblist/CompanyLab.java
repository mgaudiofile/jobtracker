package com.dev.mgaudiofile.joblist;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.dev.mgaudiofile.joblist.database.CompanyBaseHelper;
import com.dev.mgaudiofile.joblist.database.CompanyCursorWrapper;
import com.dev.mgaudiofile.joblist.database.JobsDbSchema.JobsTable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CompanyLab {
    private static CompanyLab sCompanyLab;

    private SQLiteDatabase mDataBase;

    public static CompanyLab get(Context context) {
        if (sCompanyLab == null) {
            sCompanyLab = new CompanyLab(context);
        }
        return sCompanyLab;
    }

    private CompanyLab(Context context) {
        Context mContext = context.getApplicationContext();
        mDataBase = new CompanyBaseHelper(mContext).getWritableDatabase();
    }

    public List<Company> getCompanies() {
        List<Company> companies = new ArrayList<>();
        try (CompanyCursorWrapper cursor = queryCompanies(null, null)) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                companies.add(cursor.getCompany());
                cursor.moveToNext();
            }
        }
        return companies;
    }

    public void addCompany(Company c) {
        ContentValues values = getContentValues(c);

        mDataBase.insert(JobsTable.NAME, null, values);
    }

    public Company getCompany(UUID id) {
        try (CompanyCursorWrapper cursor = queryCompanies(JobsTable.Cols.UUID + " = ?", new String[]{id.toString()})) {
            if (cursor.getCount() == 0)
                return null;
            cursor.moveToFirst();
            return cursor.getCompany();
        }
    }

    public void updateCompany(Company company) {
        String uuid = company.getId().toString();
        ContentValues values = getContentValues(company);

        mDataBase.update(JobsTable.NAME, values, JobsTable.Cols.UUID + " = ?", new String[]{ uuid } );    //? prevents injection attack
    }

    public void deleteCompany(Company company) {
        String uuid = company.getId().toString();

        mDataBase.delete(JobsTable.NAME, JobsTable.Cols.UUID + " = ?", new String[]{ uuid } );
    }

    private CompanyCursorWrapper queryCompanies(String whereClause, String[] whereArgs) {
        @SuppressLint("Recycle") Cursor cursor = mDataBase.query(
                JobsTable.NAME,
                null,       //null gets all columns
                whereClause,
                whereArgs,
                null,
                null,
                null
        );
        return new CompanyCursorWrapper(cursor);
    }

    private static ContentValues getContentValues(Company company) {
        ContentValues values = new ContentValues();
        values.put(JobsTable.Cols.UUID, company.getId().toString());
        values.put(JobsTable.Cols.COMPANY, company.getCompanyName());
        values.put(JobsTable.Cols.POSITION, company.getJobTitle());
        values.put(JobsTable.Cols.DATE_APPLIED, company.getDateApplied());
        values.put(JobsTable.Cols.STATUS, company.getStatus());
        values.put(JobsTable.Cols.STAR_FLAG, company.getStarFlag());
        values.put(JobsTable.Cols.SKILLS, company.getSkillsDesired());
        values.put(JobsTable.Cols.NOTES, company.getCompanyNotes());
        return values;
    }
}
