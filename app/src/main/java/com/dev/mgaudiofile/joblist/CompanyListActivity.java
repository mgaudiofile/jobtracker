package com.dev.mgaudiofile.joblist;

import android.support.v4.app.Fragment;

public class CompanyListActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return new CompanyListFragment();
    }
}
