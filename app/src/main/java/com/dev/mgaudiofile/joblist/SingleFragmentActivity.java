package com.dev.mgaudiofile.joblist;

import android.arch.lifecycle.ViewModelProviders;
import android.content.ClipData;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public abstract class SingleFragmentActivity extends AppCompatActivity {

    private AdView mAdView;

    private DrawerLayout mDrawerLayout;
    private SharedViewModel mSharedViewModel;
    private ClipData.Item mDrawerSelection;

    protected abstract Fragment createFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        MobileAds.initialize(this, getString(R.string.ad_mob_job_list_app_id));     //initialize with app id
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)            //remove when releasing final version
                .build();
        mAdView.loadAd(adRequest);

        mSharedViewModel = ViewModelProviders.of(this).get(SharedViewModel.class);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_24dp);
        }

        mDrawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                (MenuItem menuItem) -> {
                    // set item as selected to persist highlight
                    menuItem.setChecked(true);
                    // close drawer when item is tapped
                    mDrawerLayout.closeDrawers();

                    // Add code here to update the UI based on the item selected
                    // For example, swap UI fragments here
                    switch (menuItem.getItemId()) {
                        case R.id.drawer_item_sort_default:
                            mDrawerSelection = new ClipData.Item(getString(R.string.drawer_item_sort_default));
                            mSharedViewModel.select(mDrawerSelection);
                            return true;
                        case R.id.drawer_item_sort_favorite:
                            mDrawerSelection = new ClipData.Item(getString(R.string.drawer_item_sort_favorite));
                            mSharedViewModel.select(mDrawerSelection);
                            return true;
                        case R.id.drawer_item_sort_recent_date:
                            mDrawerSelection = new ClipData.Item(getString(R.string.drawer_item_sort_recent_date));
                            mSharedViewModel.select(mDrawerSelection);
                            return true;
                        case R.id.drawer_item_sort_status:
                            mDrawerSelection = new ClipData.Item(getString(R.string.drawer_item_sort_status));
                            mSharedViewModel.select(mDrawerSelection);
                            return true;
                        case R.id.drawer_item_sort_company:
                            mDrawerSelection = new ClipData.Item(getString(R.string.drawer_item_sort_company));
                            mSharedViewModel.select(mDrawerSelection);
                            return true;
                        case R.id.drawer_item_privacy_link:
                            openURL(getString(R.string.url_privacy_policy));
                            return true;
                        case R.id.drawer_item_terms_link:
                            openURL(getString(R.string.url_terms_conditions));
                            return true;
                    }
                    return true;    //public boolean onNavigationItemSelected(Menu item)
                });

        FragmentManager fm = getSupportFragmentManager();                       //get fragment manager
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);       //get the fragment

        if (fragment == null) {                             //if container is not holding any fragment
            fragment = createFragment();                    //call createFragment()
            fm.beginTransaction()
                    .add(R.id.fragment_container, fragment)     //resource id from activity_fragment
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /** Called when leaving the activity */
    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    /** Called when returning to the activity */
    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    /** Called before the activity is destroyed */
    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    private void openURL(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
    }
}
