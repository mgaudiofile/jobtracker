package com.dev.mgaudiofile.joblist;

import android.content.Context;

import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;

public class Filter {

    private Context mContext;
    private List<Company> mCompanies;

    Filter(Context context) {
        mContext = context;
        CompanyLab companyLab = CompanyLab.get(mContext);
        mCompanies = companyLab.getCompanies();
        Collections.reverse(mCompanies);
    }

    public List<Company> getDefaultSort() {
        CompanyLab companyLab = CompanyLab.get(mContext);       //get the singleton company lab
        List<Company> companies = companyLab.getCompanies();
        Collections.reverse(companies);
        return companies;                       //get the companies list straight from DB
    }

    public List<Company> getStarFavSort() {
        Collections.sort(mCompanies, (company, t1) -> Integer.compare(t1.getStarFlag(), company.getStarFlag()));
        return mCompanies;
    }

    public List<Company> getRecentDateSort() {
        Collections.sort(mCompanies, (company, t1) -> {
            if (company.getDateApplied() != null || company.getDateApplied().length() > 0) {
                if (t1.getDateApplied() != null || t1.getDateApplied().length() > 0) {
                    String[] date1 = company.getDateApplied().split("/");
                    String[] date2 = t1.getDateApplied().split("/");

                    int month1 = Integer.parseInt(date1[0]);
                    int day1 = Integer.parseInt(date1[1]);
                    int year1 = Integer.parseInt(date1[2]);
                    int month2 = Integer.parseInt(date2[0]);
                    int day2 = Integer.parseInt(date2[1]);
                    int year2 = Integer.parseInt(date2[2]);

                    Calendar c1 = new GregorianCalendar(year1, month1, day1);
                    Calendar c2 = new GregorianCalendar(year2, month2, day2);

                    if (c1.before(c2))
                        return 1;
                    else if (c1.after(c2))
                        return -1;
                    else
                        return 0;
                }
            }
            return 0;
        });
        return mCompanies;
    }

    public List<Company> getStatusSort() {
        Collections.sort(mCompanies, (company, t1) -> {
            if (company.getStatusHelper() != 0) {
                if (t1.getStatusHelper() != 0) {
                    return Integer.compare(company.getStatusHelper(), t1.getStatusHelper());
                }
            }
            return 0;
        });
        return mCompanies;
    }

    public List<Company> getCompanySort() {
        Collections.sort(mCompanies, (company, t1) -> {
            if (company.getCompanyName() != null) {
                if (t1.getCompanyName() != null) {
                    return company.getCompanyName().compareTo(t1.getCompanyName());
                }
            }
            return 0;
        });
        return mCompanies;
    }
}
