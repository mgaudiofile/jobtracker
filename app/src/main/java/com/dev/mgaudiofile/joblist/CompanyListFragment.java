package com.dev.mgaudiofile.joblist;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;
import java.util.Objects;

public class CompanyListFragment extends Fragment {
    //private static final String TAG = "CompanyListFragment";

    private RecyclerView mCompanyRecyclerView;      //lives in fragment_company_list.xml
    private CompanyAdapter mCompanyAdapter;

    private String mDrawerSortSelection;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        initializeSortSelect();

        SharedViewModel mSharedViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(SharedViewModel.class);
        mSharedViewModel.getSelected().observe(this, item -> {
            //update ui
            assert item != null;
            String drawer_item_sort = item.getText().toString();
            if (drawer_item_sort.equals(getString(R.string.drawer_item_sort_default)))
                mDrawerSortSelection = getString(R.string.drawer_item_sort_default);
            else if (drawer_item_sort.equals(getString(R.string.drawer_item_sort_favorite)))
                mDrawerSortSelection = getString(R.string.drawer_item_sort_favorite);
            else if (drawer_item_sort.equals(getString(R.string.drawer_item_sort_recent_date)))
                mDrawerSortSelection = getString(R.string.drawer_item_sort_recent_date);
            else if (drawer_item_sort.equals(getString(R.string.drawer_item_sort_status)))
                mDrawerSortSelection = getString(R.string.drawer_item_sort_status);
            else if (drawer_item_sort.equals(getString(R.string.drawer_item_sort_company)))
                mDrawerSortSelection = getString(R.string.drawer_item_sort_company);
            updateUI(mDrawerSortSelection);
        });
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {
        View view = inflater.inflate(R.layout.fragment_company_list, container, false);     //inflate the view

        mCompanyRecyclerView = view.findViewById(R.id.company_recycler_view);               //use inflated view to get recycler view by id
        mCompanyRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));      //RecyclerView requires a layout manager
        DividerItemDecoration dividerDecoration = new DividerItemDecoration(Objects.requireNonNull(getActivity()).getApplicationContext(),
                                                                            LinearLayoutManager.VERTICAL);
        mCompanyRecyclerView.addItemDecoration(dividerDecoration);

        initializeSortSelect();

        updateUI(mDrawerSortSelection);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI(mDrawerSortSelection);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(R.menu.fragment_company_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_add_job:
                Company company = new Company();
                CompanyLab.get(getActivity()).addCompany(company);
                Intent intent = CompanyPagerActivity.newIntent(getActivity(), company.getId(), true);
                startActivity(intent);
                return true;

                default:
                    return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Preferences.setStoredSortSelect(getContext(), mDrawerSortSelection);
    }

    private void updateSubtitle() {                                 //shows the count of companies as subtitle, need to make it auto
        CompanyLab companyLab = CompanyLab.get(getActivity());
        int companiesCount = companyLab.getCompanies().size();
        String subtitle = getString(R.string.subtitle_format, companiesCount);

        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        assert appCompatActivity != null;
        Objects.requireNonNull(appCompatActivity.getSupportActionBar()).setSubtitle(subtitle);
    }

    private void updateUI(String sort) {
        CompanyLab companyLab = CompanyLab.get(getActivity());
        List<Company> companies = companyLab.getCompanies();            //gets list from db
        Filter mFilter = new Filter(getActivity());

        if (sort.equals(getString(R.string.drawer_item_sort_default)))
            companies = mFilter.getDefaultSort();
        else if (sort.equals(getString(R.string.drawer_item_sort_favorite)))
            companies = mFilter.getStarFavSort();
        else if (sort.equals(getString(R.string.drawer_item_sort_recent_date)))
            companies = mFilter.getRecentDateSort();
        else if (sort.equals(getString(R.string.drawer_item_sort_status)))
            companies = mFilter.getStatusSort();
        else if (sort.equals(getString(R.string.drawer_item_sort_company)))
            companies = mFilter.getCompanySort();

        if (mCompanyAdapter == null) {
            mCompanyAdapter = new CompanyAdapter(companies);            //instantiate a new adapter with the companies list
            mCompanyRecyclerView.setAdapter(mCompanyAdapter);           //set the adapter to the recycler view
        } else {
            mCompanyAdapter.setCompanies(companies);
            mCompanyAdapter.notifyDataSetChanged();
            mCompanyRecyclerView.smoothScrollToPosition(0);
        }
        updateSubtitle();
    }

    private void initializeSortSelect() {
        mDrawerSortSelection = (Preferences.getStoredSortSelect(getContext()) != null)
                ? Preferences.getStoredSortSelect(getContext())
                : getString(R.string.drawer_item_sort_default);
    }

    //View Holder
    private class CompanyHolder extends RecyclerView.ViewHolder
                                implements View.OnClickListener {

        private TextView mCompanyTextView;
        private TextView mPositionTextView;
        private TextView mDateAppliedTextView;
        private TextView mStatusTextView;
        private ImageButton mStarFavoritesImageButton;

        private Company mCompany;
        private boolean mButtonPressed;

        CompanyHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.list_item_company, parent, false));
            itemView.setOnClickListener(this);                                                      //itemView is the view for entire row, set a click listener

            mCompanyTextView = itemView.findViewById(R.id.label_company);
            mPositionTextView = itemView.findViewById(R.id.label_position);
            mDateAppliedTextView = itemView.findViewById(R.id.textView_date_applied);
            mStatusTextView = itemView.findViewById(R.id.textView_status);

            mStarFavoritesImageButton = itemView.findViewById(R.id.imageButton_star);
            //click listener for star button
            mStarFavoritesImageButton.setOnClickListener(view -> {
                if (!mButtonPressed) {
                    mStarFavoritesImageButton.setImageResource(R.drawable.ic_star_filled_24dp);
                    mCompany.setStarFlag(1);
                    CompanyLab.get(getActivity()).updateCompany(mCompany);                              //updating DB!!
                    mButtonPressed = true;
                } else {
                    mStarFavoritesImageButton.setImageResource(R.drawable.ic_star_border_grey_24dp);
                    mCompany.setStarFlag(0);
                    CompanyLab.get(getActivity()).updateCompany(mCompany);                              //updating DB!!
                    mButtonPressed = false;
                }
            });
        }

        @Override
        public void onClick(View view) {
            Intent intent = CompanyPagerActivity.newIntent(getActivity(), mCompany.getId(), false);
            startActivity(intent);
        }

        public void bind(Company company) {                         //bind is called every time a company is displayed in CompanyHolder
            mCompany = company;
            if (mCompany.getCompanyName() != null && !mCompany.getCompanyName().equals(""))
                mCompanyTextView.setText(mCompany.getCompanyName());                                //test set text
            else if (mCompany.getCompanyName() != null && mCompany.getCompanyName().equals(""))
                mCompanyTextView.setText(R.string.company_name);

            if (mCompany.getJobTitle() != null && !mCompany.getJobTitle().equals(""))
                mPositionTextView.setText(mCompany.getJobTitle());
            else if (mCompany.getJobTitle() != null && mCompany.getJobTitle().equals(""))
                mPositionTextView.setText(R.string.position_title);

            if (mCompany.getDateApplied() != null && !mCompany.getDateApplied().equals(""))
                mDateAppliedTextView.setText(mCompany.getDateApplied());

            if (mCompany.getStatus() != null && !mCompany.getStatus().equals("Select"))
                mStatusTextView.setText(mCompany.getStatus());
            else
                mStatusTextView.setText("");        //select is current so clear the textView

            if (mCompany.getStarFlag() == 1)
                mStarFavoritesImageButton.setImageResource(R.drawable.ic_star_filled_24dp);
            else if (mCompany.getStarFlag() == 0)
                mStarFavoritesImageButton.setImageResource(R.drawable.ic_star_border_grey_24dp);
        }
    }

    //Adapter
    private class CompanyAdapter extends RecyclerView.Adapter<CompanyHolder> {

        private List<Company> mCompanies;

        CompanyAdapter(List<Company> companies) {
            mCompanies = companies;
        }

        @NonNull
        @Override
        public CompanyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());

            return new CompanyHolder(layoutInflater, parent);
        }

        @Override
        public void onBindViewHolder(@NonNull CompanyHolder holder, int position) {      //must be efficient for smooth scrolling
            Company company = mCompanies.get(position);
            holder.bind(company);
        }

        @Override
        public int getItemCount() {
            return mCompanies.size();
        }

        void setCompanies(List<Company> companies) {
            mCompanies = companies;
        }
    }
}
