package com.dev.mgaudiofile.joblist.database;

public class JobsDbSchema {

    public static final class JobsTable {
        public static final String NAME = "companies";       //name of table

        public static final class Cols {
            public static final String UUID = "uuid";
            public static final String COMPANY = "company";
            public static final String POSITION = "position";
            public static final String DATE_APPLIED = "date_applied";
            public static final String STATUS = "status";
            public static final String STAR_FLAG = "star_flag";
            public static final String SKILLS = "skills_desired";
            public static final String NOTES = "notes";
        }
    }
}
