package com.dev.mgaudiofile.joblist;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class CompanyFragment extends Fragment {
    //private static final String TAG = "CompanyFragment";

    private static final String ARG_COMPANY_ID = "company_id";
    private static final String ARG_ENABLE_ADD_JOB = "enable_add_job";

    private Company mCompany;
    private EditText mCompanyNameEditText;
    private EditText mJobTitleEditText;

    private TextView mDateDisplayTextView;
    private ImageButton mDatePickerImageButton;

    private Spinner mStatusSpinner;

    private EditText mSkillsDesiredEditText;
    private EditText mNotesEditText;

    private MenuItem mEditMenuItem;
    private MenuItem mSaveMenuItem;
    private MenuItem mCancelMenuItem;
    private MenuItem mDeleteMenuItem;

    private boolean isAddJob;   //to detect if user navigated from add job vs. click item view

    private List<String> stateList = new ArrayList<>();

    public static CompanyFragment newInstance(UUID companyId, boolean isAddJob) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_COMPANY_ID, companyId);
        args.putBoolean(ARG_ENABLE_ADD_JOB, isAddJob);

        CompanyFragment fragment = new CompanyFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        UUID companyId = (UUID) getArguments().getSerializable(ARG_COMPANY_ID);
        isAddJob = getArguments().getBoolean(ARG_ENABLE_ADD_JOB);
        mCompany = CompanyLab.get(getActivity()).getCompany(companyId);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        CompanyLab.get(getActivity()).updateCompany(mCompany);
        if (isNullJob()) {
            deleteFrag();       //lifecycle methods getting called twice because of view pager
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(R.menu.fragment_company, menu);
        mEditMenuItem = menu.findItem(R.id.menu_item_edit_info);
        mSaveMenuItem = menu.findItem(R.id.menu_item_save_info);
        mCancelMenuItem = menu.findItem(R.id.menu_item_cancel);
        mDeleteMenuItem = menu.findItem(R.id.menu_item_delete);

        if (isAddJob)
            enableEditMenu();
        else
            disableEditMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                readInfo();
                if (isNullJob()) {
                    deleteFrag();
                }
                NavUtils.navigateUpFromSameTask(Objects.requireNonNull(getActivity()));
                return true;
            case R.id.menu_item_edit_info:
                enableEditMenu();
                enableEdit();
                writeToList();
                return true;
            case R.id.menu_item_save_info:
                readInfo();
                if (isNullJob()) {
                    deleteFrag();
                }
                Objects.requireNonNull(getActivity()).finish();
                return true;
            case R.id.menu_item_cancel:
                refreshState();
                disableEditMenu();
                disableEdit();
                return true;
            case R.id.menu_item_delete:
                DeleteForeverDialogFragment dialogFragment = DeleteForeverDialogFragment.newInstance(mCompany);
                dialogFragment.setTargetFragment(this, 0);
                assert getFragmentManager() != null;
                dialogFragment.show(getFragmentManager().beginTransaction(), "test");
                //deleteFrag();
                //getActivity().finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_company, container, false);                 //inflate the fragment_company layout and retrieve the views

        mCompanyNameEditText = v.findViewById(R.id.editText_company_name);
        mCompanyNameEditText.setText(mCompany.getCompanyName());

        mJobTitleEditText = v.findViewById(R.id.editText_job_title);
        mJobTitleEditText.setText(mCompany.getJobTitle());

        mDateDisplayTextView = v.findViewById(R.id.textView_date_display);
        mDateDisplayTextView.setText(mCompany.getDateApplied());

        mDatePickerImageButton = v.findViewById(R.id.imageButton_date_picker);
        mDatePickerImageButton.setOnClickListener(view -> {
            DatePickerFragment mDatePickerFrag = new DatePickerFragment();
            mDatePickerFrag.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), "Date Picker");
        });

        mStatusSpinner = v.findViewById(R.id.spinner_status);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                                                                    Objects.requireNonNull(getContext()),
                                                                    R.array.status_array, R.layout.custom_spinner_layout);
        adapter.setDropDownViewResource(R.layout.custom_spinner_layout);
        mStatusSpinner.setAdapter(adapter);
        mStatusSpinner.setSelection(mCompany.getStatusHelper());

        mSkillsDesiredEditText = v.findViewById(R.id.editText_skills_desired);
        mSkillsDesiredEditText.setText(mCompany.getSkillsDesired());

        mNotesEditText = v.findViewById(R.id.editText_notes);
        mNotesEditText.setText(mCompany.getCompanyNotes());

        if (isAddJob)
            enableEdit();
        else
            disableEdit();

        return v;
    }

    private void disableEdit() {
        mCompanyNameEditText.setEnabled(false);
        mJobTitleEditText.setEnabled(false);
        mDatePickerImageButton.setEnabled(false);
        mStatusSpinner.setEnabled(false);
        mSkillsDesiredEditText.setEnabled(false);
        mNotesEditText.setEnabled(false);
    }

    private void enableEdit() {
        mCompanyNameEditText.setEnabled(true);
        mJobTitleEditText.setEnabled(true);
        mDatePickerImageButton.setEnabled(true);
        mStatusSpinner.setEnabled(true);
        mSkillsDesiredEditText.setEnabled(true);
        mNotesEditText.setEnabled(true);
    }

    private void enableEditMenu() {
        mEditMenuItem.setVisible(false);
        mSaveMenuItem.setVisible(true);
        mCancelMenuItem.setVisible(true);
        mDeleteMenuItem.setVisible(true);
    }

    private void disableEditMenu() {
        mEditMenuItem.setVisible(true);
        mSaveMenuItem.setVisible(false);
        mCancelMenuItem.setVisible(false);
        mDeleteMenuItem.setVisible(false);
    }

    private void writeToList() {
        stateList.add(mCompanyNameEditText.getText().toString());
        stateList.add(mJobTitleEditText.getText().toString());
        stateList.add(mDateDisplayTextView.getText().toString());
        stateList.add(mSkillsDesiredEditText.getText().toString());
        stateList.add(mNotesEditText.getText().toString());
    }

    private void refreshState() {
        if (stateList.size() >= 5) {
            mCompanyNameEditText.setText(stateList.get(0));
            mJobTitleEditText.setText(stateList.get(1));
            mDateDisplayTextView.setText(stateList.get(2));
            mSkillsDesiredEditText.setText(stateList.get(3));
            mNotesEditText.setText(stateList.get(4));
        }
    }

    private void readInfo() {
        //save all the updated information
        if (mCompanyNameEditText.getText() != null)
            mCompany.setCompanyName(mCompanyNameEditText.getText().toString());

        if (mJobTitleEditText.getText() != null)
            mCompany.setJobTitle(mJobTitleEditText.getText().toString());

        if (mDateDisplayTextView.getText() != null) {
            mCompany.setDateApplied(mDateDisplayTextView.getText().toString());
        }
        mCompany.setStatus(mStatusSpinner.getSelectedItem().toString());

        if (mSkillsDesiredEditText.getText() != null)
            mCompany.setSkillsDesired(mSkillsDesiredEditText.getText().toString());

        if (mNotesEditText.getText() != null)
            mCompany.setCompanyNotes(mNotesEditText.getText().toString());
    }

    private boolean isNullJob() {
        if (!(mCompany.getCompanyName() == null || mCompany.getCompanyName().equals("")))
            return false;
        if (!(mCompany.getJobTitle() == null || mCompany.getJobTitle().equals("")))
            return false;
        if (!(mCompany.getDateApplied() == null || mCompany.getDateApplied().equals("")))
            return false;
        if (!(mCompany.getStatus() == null || mCompany.getStatus().equals("Select")))
            return false;
        if (!(mCompany.getSkillsDesired() == null || mCompany.getSkillsDesired().equals("")))
            return false;
        if (!(mCompany.getCompanyNotes() == null || mCompany.getCompanyNotes().equals("")))
            return false;
        return true;
    }

    private void deleteFrag() {
        List<Company> mCompanies = CompanyLab.get(getActivity()).getCompanies();
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        fm.beginTransaction().remove(this).commit();
        mCompanies.remove(mCompany);                                                //probably unnecessary
        CompanyLab.get(getActivity()).deleteCompany(mCompany);                      //delete from database
        ((CompanyPagerActivity) getActivity()).dataChanged();
    }

    public static class DeleteForeverDialogFragment extends DialogFragment {

        public static DeleteForeverDialogFragment newInstance(Company company) {
            DeleteForeverDialogFragment dialogFragment = new DeleteForeverDialogFragment();
            Bundle args = new Bundle();
            args.putSerializable("company_id", company.getId());        //currently not used
            dialogFragment.setArguments(args);
            return dialogFragment;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
            builder.setMessage(R.string.delete_message)
                    .setPositiveButton(R.string.yes, (dialog, id) -> {
                        assert getTargetFragment() != null;
                        ((CompanyFragment)getTargetFragment()).deleteFrag();
                        getActivity().finish();                                                 //exit the activity
                    })
                    .setNegativeButton(R.string.no, (dialog, id) -> {
                        // User cancelled the dialog, do nothing
                    });
            // Create the AlertDialog object and return it
            return builder.create();
        }
    }
}
