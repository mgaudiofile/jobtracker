package com.dev.mgaudiofile.joblist;

import android.content.Context;
import android.preference.PreferenceManager;

public class Preferences {
    private static final String PREF_SORT_SELECT = "sortSelection";

    public static String getStoredSortSelect(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(PREF_SORT_SELECT, null);                 //null is default value
    }

    public static void setStoredSortSelect(Context context, String sortSelection) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString(PREF_SORT_SELECT, sortSelection)
                .apply();
    }
}
