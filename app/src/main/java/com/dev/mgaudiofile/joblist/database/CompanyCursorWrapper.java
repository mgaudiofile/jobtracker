package com.dev.mgaudiofile.joblist.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.dev.mgaudiofile.joblist.Company;
import com.dev.mgaudiofile.joblist.database.JobsDbSchema.JobsTable;

import java.util.UUID;

public class CompanyCursorWrapper extends CursorWrapper {

    public CompanyCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Company getCompany() {
        String uuidString = getString(getColumnIndex(JobsTable.Cols.UUID));
        String company = getString(getColumnIndex(JobsTable.Cols.COMPANY));
        String position = getString(getColumnIndex(JobsTable.Cols.POSITION));
        String dateApplied = getString(getColumnIndex(JobsTable.Cols.DATE_APPLIED));
        String status = getString(getColumnIndex(JobsTable.Cols.STATUS));
        int starFlag = getInt(getColumnIndex(JobsTable.Cols.STAR_FLAG));
        String skillsDesired = getString(getColumnIndex(JobsTable.Cols.SKILLS));
        String notes = getString(getColumnIndex(JobsTable.Cols.NOTES));

        Company c = new Company(UUID.fromString(uuidString));
        c.setCompanyName(company);
        c.setJobTitle(position);
        c.setDateApplied(dateApplied);
        c.setStatus(status);
        c.setStarFlag(starFlag);
        c.setSkillsDesired(skillsDesired);
        c.setCompanyNotes(notes);

        return c;
    }
}
