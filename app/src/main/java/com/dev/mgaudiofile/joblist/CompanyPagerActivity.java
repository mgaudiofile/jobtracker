package com.dev.mgaudiofile.joblist;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class CompanyPagerActivity extends AppCompatActivity {

    private static final String EXTRA_COMPANY_ID = "com.dev.mgaudiofile.joblist.company_id";
    private static final String EXTRA_IS_ADD_JOB = "com.dev.mgaudiofile.joblist.is_add_job";

    private ViewPager mViewPager;
    private List<Company> mCompanies;

    public static Intent newIntent(Context packageContext, UUID companyId, boolean isAddJob) {
        Intent intent = new Intent(packageContext, CompanyPagerActivity.class);
        intent.putExtra(EXTRA_COMPANY_ID, companyId);
        intent.putExtra(EXTRA_IS_ADD_JOB, isAddJob);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_pager);

        Toolbar toolbar = findViewById(R.id.include_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar == null)
            throw new AssertionError();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(R.string.toolbar_details_title);

        UUID companyId = (UUID) getIntent().getSerializableExtra(EXTRA_COMPANY_ID);
        final boolean isAddJob = getIntent().getBooleanExtra(EXTRA_IS_ADD_JOB, false);

        mViewPager = findViewById(R.id.company_view_pager);

        mCompanies = CompanyLab.get(this).getCompanies();
        FragmentManager fragmentManager = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {

            @Override
            public Fragment getItem(int position) {
                Company company = mCompanies.get(position);
                return CompanyFragment.newInstance(company.getId(), isAddJob);  //final causes the ide to underline the variable
            }

            @Override
            public int getCount() {
                return mCompanies.size();
            }
        });

        for(int i = 0; i < mCompanies.size(); i++) {
            if (mCompanies.get(i).getId().equals(companyId)) {
                mViewPager.setCurrentItem(i);
                break;
            }
        }
    }

    public void dataChanged() {
        Objects.requireNonNull(mViewPager.getAdapter()).notifyDataSetChanged();
    }
}
