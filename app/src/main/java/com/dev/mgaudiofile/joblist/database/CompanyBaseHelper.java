package com.dev.mgaudiofile.joblist.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.dev.mgaudiofile.joblist.database.JobsDbSchema.JobsTable;

public class CompanyBaseHelper extends SQLiteOpenHelper {               //helps to open SQLite DB
    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "companyBase.db";

    public CompanyBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {                       //uninstall app to destroy db and create new one
        db.execSQL("create table " + JobsTable.NAME + "(" +
            " _id integer primary key autoincrement, " +
            JobsTable.Cols.UUID + ", " +
            JobsTable.Cols.COMPANY + ", " +
            JobsTable.Cols.POSITION + ", " +
            JobsTable.Cols.DATE_APPLIED + ", " +
            JobsTable.Cols.STATUS + ", " +
            JobsTable.Cols.STAR_FLAG + ", " +
            JobsTable.Cols.SKILLS + ", " +
            JobsTable.Cols.NOTES + ")"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
